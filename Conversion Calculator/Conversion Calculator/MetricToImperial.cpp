#include <iostream>
#include <string>
#include "MetricToImperial.h"

using namespace std;

int numberToConvert;
int conversionDecicion;

MetricToImperial::Centimeter replayGame;

  bool MetricToImperial::askToContinue() { // TODO have continueConverting be a yes/no confirmation
/*	string continueConverting;
	cout << "Would you like to convert something else?" << endl;
	getline(cin, continueConverting);
	cin >> continueConverting;
	cout << continueConverting;*/

	cout << "Would you like to continue converting? (y/n) ";
	string Response = "";
	cin >> Response;
	cout << Response << std::endl;
//	string test;
//	cin >> test;
	return (Response[0] == 'y') || (Response[0] == 'Y');

}

void MetricToImperial::confirmContinue() {
	do {
		introduction();
		replayGame.getInstruction();
		replayGame.checkInstruction();
	} while (askToContinue() == 1);
}

void MetricToImperial::introduction() {
	cout << "What would you like to convert? (type the number that applies)" << endl;
	cout << "1: Centimeters to inches" << endl;
	cout << "2: Centimeters to feet" << endl;
}

void MetricToImperial::Centimeter::centimetersToInches() 
{
	cout << "Converting centimeters to inches" << endl;
	cout << "What number would you like to convert?" << endl;

	cin >> numberToConvert;
	cout << numberToConvert << " centimeter(s) in inches is: " << numberToConvert * 0.393701 << endl;
	cout << endl;
}

void MetricToImperial::Centimeter::centimetersToFeet()
{
	cout << "Converting centimeters to feet" << endl;
	cout << "What number would you like to convert?" << endl << endl;

	cin >> numberToConvert;
	cout << numberToConvert << " centimeter(s) in feet is: " << numberToConvert * 0.0328084 << endl;
	cout << endl;
}

void MetricToImperial::Centimeter::getInstruction()
{
	cin >> conversionDecicion;
	cout << endl;
}

void MetricToImperial::Centimeter::checkInstruction()
{
	if (conversionDecicion == 1) {
		centimetersToInches();
	}

	else if (conversionDecicion == 2) {
		centimetersToFeet();
	}

	else if (conversionDecicion == 3) {
		centimetersToFeet();
	}

	else {
		cout << "Invalid entry. Please submit a number." << endl;
	}

	//	else {
	//		cout << "Invalid entry. Please enter a number: ";
	//	}

		/*
			switch (sentence) {
				case 1: {
					centimetersToInches();
					break;
				}

				case 2: {
					centimetersToFeet();
					break;
				}

				default: {
					cout << "Invalid entry. Please enter a number: ";
					break;
				}

			}
			*/

}