#pragma once
#pragma once
#include <iostream>
#include <string>

class MetricToImperial {
public:
	bool askToContinue();
	void confirmContinue();
	void introduction();
	class Centimeter {
	public:
		void centimetersToInches();
		void centimetersToFeet();
		void getInstruction();
		void checkInstruction();
	};

private:
};