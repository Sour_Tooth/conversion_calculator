#include <iostream>
#include <string>
#include "MetricToImperial.h"

using namespace std;

MetricToImperial::Centimeter centimeterConversion;
MetricToImperial globalCommands;

int main() {

	globalCommands.introduction();
	centimeterConversion.getInstruction();
	centimeterConversion.checkInstruction();
	globalCommands.askToContinue();


	string end;
	cin >> end;
}
